#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASH_LENGTH 26

int length(char* string){
    int counter = 0;
    while(*string){
        ++counter;
        ++string;
    }
    return counter;
}


char* hashe(char *string_to_hash){

    char* hash_result = (char*) malloc(HASH_LENGTH);
    memset(hash_result, 0, HASH_LENGTH);

    while(*string_to_hash){
        hash_result[(*string_to_hash) - 'a'] = 1;
        ++string_to_hash;
    }

    return hash_result;
}


char* delete(char *input_string, int length, char *hashed_string){

    char* result = (char*) malloc(length);
    memset(result, 0, length);
    int counter = 0;

    for(int i = 0; i < length; ++i){
        if(!hashed_string[input_string[i] - 'a']){
            result[counter] = input_string[i];
            ++counter;
        }
    }

    return result;
}


int main(int argc, char* argv[]){

    if(argc < 3){
        printf("Need 2 strings!");
        exit(0);
    }

    char* first_s = argv[1];
    char* second_s = argv[2];

    printf("1. Input: %s\n2. Input: %s\n", first_s, second_s);

    int first_l = length(first_s);
    int second_l = length(second_s);

    printf("Hashing...\n");
    char* hashed_string = hashe(second_s);
    printf("Deleting...\n");
    char* result_string = delete(first_s, first_l, hashed_string);

    printf("Result: %s", result_string);

    free(hashed_string);
    free(result_string);
}
